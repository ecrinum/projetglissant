from flask import Flask, render_template, redirect 
import requests
import json
import links_from_header
import re

glissant = Flask(__name__, template_folder='./templates/')

group= '2122221'

group_qc = '2434503'
bibstyle='chicago-fullnote-bibliography-fr'

# fonction pour construire la home page
@glissant.route('/') 
def home():
    data='Bienvenue sur le site des études sur Glissant'
    return render_template('index.html',data=data)
@glissant.route('/allitems') 
def allitems():


    key = 'S4WUvmkvLi03MK4WoQTqbcBR'
    
    url = 'https://api.zotero.org/groups/'
    
    params={
        'key':key,
        'start':'0'
        
    }
    
    pagination= True
    
    bibliographical_data=[]
    while pagination:
    
        data = requests.get(url+group+'/items', params)
        data_headers = data.headers
    
        data_link=data_headers['Link']
        data_link_header=links_from_header.extract(data_link) # using links_from_headers to tranform Link value in a json with the keys prev next last
    
        if 'next' in data_link_header:
            next_link=data_link_header['next'] # picking the value of the key last 
    
            next_start=re.match( r'.*start=([0-9]*)', next_link).group(1)
            params['start']= next_start
    
            biblio_data=data.json()
            for bib_item in biblio_data:
                bibliographical_data.append(bib_item)
            
        else:
            pagination=False
    return render_template('allitems.html',data=bibliographical_data)
    
@glissant.route('/<item_key>') 
def bibitem(item_key):
    key = 'S4WUvmkvLi03MK4WoQTqbcBR'
    
    params={
        'key':key,
        'include':'citation,data',
        'style': bibstyle, 
    }
    url = 'https://api.zotero.org/groups/'
    data = requests.get(url+group+'/items/'+item_key, params).json() 

    data_children= requests.get(url+group+'/items/'+item_key+'/children', params).json()
    
    if data['data']['relations']:
        data_linked=[]
        if isinstance((data['data']['relations']['dc:relation']), list):
            #print('LIST!!!')
                
            for item in data['data']['relations']['dc:relation']:
                linked_item_key= item.replace('http://zotero.org/groups/2122221/items/', '') 
                item_data_linked= requests.get(url+group+'/items/'+linked_item_key, params).json()        
                print(item)
                data_linked.append(item_data_linked)
        else:
            
            linked_item_key= data['data']['relations']['dc:relation'].replace('http://zotero.org/groups/2122221/items/', '') 
            item_data_linked= requests.get(url+group+'/items/'+linked_item_key, params).json()        
            data_linked.append(item_data_linked)
    else:
        data_linked=None

    return render_template('item.html',data=data, data_children=data_children, data_linked=data_linked)

@glissant.route('/tags') 
def tags():
    key = 'S4WUvmkvLi03MK4WoQTqbcBR'
    
    params={
        'key':key,
        
    }
    url = 'https://api.zotero.org/groups/'
    data = requests.get(url+group+'/tags', params).json() 

    return render_template('tags.html',data=data)

@glissant.route('/tags/<tag_name>') 
def tag(tag_name):
    key = 'S4WUvmkvLi03MK4WoQTqbcBR'
    
    params={
        'key':key,
        'tag':tag_name 
    }
    url = 'https://api.zotero.org/groups/'
    data = requests.get(url+group+'/items/', params).json() 
    print(url+group+'/tags/'+tag_name)

    return render_template('tag.html',data=data)

@glissant.route('/collections') 
def collections():
    key = 'S4WUvmkvLi03MK4WoQTqbcBR'
    
    params={
        'key':key,
        
    }
    url = 'https://api.zotero.org/groups/'
    data = requests.get('https://api.zotero.org/groups/2122221/collections/?key=S4WUvmkvLi03MK4WoQTqbcBR').json() 

    return render_template('collections.html',data=data)

@glissant.route('/collections/<collection_id>') 
def collection(collection_id):
    key = 'S4WUvmkvLi03MK4WoQTqbcBR'
    
    params={
        'key':key,
        'include':'citation,data',
        'style':bibstyle,
        'sort':'date'
    }
    pagination=True
    bibliographical_data=[]
    while pagination:
        url = 'https://api.zotero.org/groups/'
        url_collection=url+'/'+group+'/collections/'+collection_id+'/items/'
        datac = requests.get(url_collection, params)
        datac_headers = datac.headers
        datac_link=datac_headers['Link']
        datac_link_header=links_from_header.extract(datac_link)
        biblio_data=datac.json()
        for bib_item in biblio_data:
            bibliographical_data.append(bib_item)
        if 'next' in datac_link_header:
           next_link=datac_link_header['next'] # picking the value of the key last 
        
           next_start=re.match( r'.*start=([0-9]*)', next_link).group(1)
           params['start']= next_start
        
           
        else:
            pagination=False
    return render_template('collection.html',data=bibliographical_data)
@glissant.route('/subcollections/<collection_id>') 
def subcollections(collection_id):
    key = 'S4WUvmkvLi03MK4WoQTqbcBR'
    
    params={
        'key':key,
        
    }
    url = 'https://api.zotero.org/groups/'
    data = requests.get(url+group+'/collections/'+collection_id+'/collections/', params).json() 

    return render_template('subcollections.html',data=data)
if __name__ == '__main__':
    glissant.run(host='0.0.0.0', debug=True)
