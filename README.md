# Projet Glissant
Expérimentation pour extraire et afficher des données bibliographiques dans le cadre du projet [Glissant Studies](https://www.glissantstudies.com/).

## Pour faire fonctionner le script Python en local

1. créer un environnement virtuel : `python3 -m venv venv`
2. activer l'environnement : `source venv/bin/activate`
3. installer les dépendances : `pip3 install requirements.txt`
4. lancer le script : `python3 glissant.py`
5. le site est disponible en local à l'adresse [http://localhost:5000](http://localhost:5000/)
